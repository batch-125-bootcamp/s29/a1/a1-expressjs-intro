let express = require("express");
const port = 4000;

let app = express();

let users = [
	{"username" : "kth",
	"password" : "kth18"
	}, 
	{"username" : "jjk",
	"password" : "jjk01"
	}
];

app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.get("/home", (req, res) => res.send(`Welcome to the Homepage!`));

app.post("/signup", (req, res) => {
		if (req.body.username == "" || req.body.password == "") {
			res.send(`Please input BOTH username and password`)
		} else {
			users.push(req.body)
			res.send(`User ${req.body.username} successfully registered.`)
		}
});

app.get("/users", (req, res) => res.send(users));

app.listen(port, () =>	console.log(`Server running at port ${port}`));